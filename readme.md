## 1.
Clone this project into your localhost root folder

## 2.
Open terminal, cd to the root folder of this project:
    1. run 'npm install' to install dependencies.
    2. run 'composer install' to install dependencies.
    3. run 'sudo chmod -R 777 storage' to give permissions to storage folder
    4. run 'php artisan storage:link' to link your storage to your public assets folder.

## 3.
Under root folder of this project, run 'sudo chmod -R 777 storage' and 'sudo chmod -R 777 bootstrap/cache' to give permissions for storage and cache folder.

## 4.
Call the API by sending csv file to http://localhost/bigtincan/public/csv, the field name should be 'csv_file'.

## 5.
The result will be saved in the storage folder, under 'bigtincan_test/storage/app/public'. Two files will be generated, one is for successful records and the other one is for the records with errors.

## 6.
Go to terminal and cd to the root folder of this project then run 'vendor/bin/phpunit' to see the unit test result.
