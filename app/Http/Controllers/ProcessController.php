<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Facades\CSVService;

use Symfony\Component\HttpFoundation\Response;

class ProcessController extends Controller
{
    const FILE_GENERATED_RESPONSE_MESSAGE = 'Total successful rows: ';
    //
    public function index(Request $request) {

        $file = $request->file('csv_file');

        // check if the file is past
        if (empty($file)) {
            return response()->json([
                'success' => false,
                'message' => 'File is not provided.'
            ], Response::HTTP_NO_CONTENT);
        }

        // check the file format
        if (!empty($file->getClientOriginalExtension()) && !in_array($file->getClientOriginalExtension(), ['csv'])) {
            return response()->json([
                'success' => false,
                'message' => 'File extension is not valid.'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // get the data in the file
        $csvResult = CSVService::processCSV($file->getRealPath());

        if (empty($csvResult) || !$csvResult['success']) {
            return response()->json($csvResult, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // get the indexes of the required fields
        $indexesOfFieldsResult = CSVService::getIndexesOfFields($csvResult['message'][0]);

        if (empty($indexesOfFieldsResult) || !$indexesOfFieldsResult['success']) {
            return response()->json($indexesOfFieldsResult, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // generate json file based on the input data
        $successfulRowNum = CSVService::generateJSON($csvResult['message'], $indexesOfFieldsResult['message'], 'sample_users_successful.json', 'sample_users_unsuccessful.json');
        return $this::FILE_GENERATED_RESPONSE_MESSAGE.$successfulRowNum;
    }
}
