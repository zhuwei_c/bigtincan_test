<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\CSVService;
use App\Services\FileService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // csv service
        $this->app->singleton('CSVService', function($app){
            return new CSVService;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
