<?php

namespace App\Services;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CSVService
{
    const REQUIRED_FIELDS = [
        'email',
        'first_name',
        'last_name',
        'password',
        'platforms'
    ];
    const EMAIL_INVALID_ERROR_MESSAGE = 'Email format is not valid.';
    const EMAIL_MISSING_ERROR_MESSAGE = 'Email field is missing.';
    const FIRST_NAME_MISSING_ERROR_MESSAGE = 'First Name field is missing.';
    const LAST_NAME_MISSING_ERROR_MESSAGE = 'Last Name field is missing.';
    const PASSWORD_INVALID_ERROR_MESSAGE = 'Password must be minimum 8 characters.';
    const PLATFORMS_INVALID_ERROR_MESSAGE = "Platforms must have one or more of 'IOS', 'Windows', 'Android' and 'Web'.";

    protected $errors;

    public function processCSV($file)
    {
        try{
            // load the file by using IOFactory
            $spreadsheet = IOFactory::load($file);
            // convert the data into array
            $tableData = $spreadsheet->getActiveSheet()->toArray();

            return [
                'success' => true,
                'message' => $tableData
            ];

        } catch (\Exception $e){
            $message = $e->getMessage();
            return [
                'success' => false,
                'message' => $message
            ];
        }
    }

    // check the index of the fields
    public function getIndexesOfFields($fields)
    {
        $result = [
            'success' => true,
            'message' => ''
        ];
        $allFields = array();

        foreach ($fields as $column_index => $column) {
            $field = trim(strtolower($column));
            $fieldWords = explode(' ', $field);
            $snakeField = implode('_', $fieldWords);
            $allFields[$snakeField] = $column_index;
        }

        foreach ($this::REQUIRED_FIELDS as $value) {

            if (!isset($allFields[$value])) { 
                $result['success'] = false;
                $result['message'] .= "Field {$value} is missing. \n";
            }

        }

        if ($result['success']) {
            $result['message'] = $allFields;
        }

        return $result;
    }

    public function generateJSON($tableData, $fields, $successfulFileName, $unsuccessfulFileName)
    {
        $this->errors = array();
        $validRows = array();
        $successfulRowNum = 0;

        foreach ($tableData as $index => $row) {

            // skip the field name row
            if ($index == 0) {
                continue;
            }

            // check row validation
            if ($this->rowValidation($index, $row, $fields)) {
                $validRows = $this->recordValidRow($index, $row, $fields, $validRows);
                $successfulRowNum++;
            }

        }

        Storage::put('public/'.$successfulFileName, json_encode($validRows));
        Storage::put('public/'.$unsuccessfulFileName, json_encode($this->errors));
        return $successfulRowNum;
    }

    public function rowValidation($rowIndex, $row, $fields)
    {
        $this->errors['Row_'.$rowIndex] = array();
        
        if (!empty($row[$fields['email']])) {

            if (!filter_var($row[$fields['email']], FILTER_VALIDATE_EMAIL)) {
                $this->errors['Row_'.$rowIndex][] = $this::EMAIL_INVALID_ERROR_MESSAGE;
            }

        } else {
            $this->errors['Row_'.$rowIndex][] = $this::EMAIL_MISSING_ERROR_MESSAGE;
        }

        if (empty($row[$fields['first_name']])) {
            $this->errors['Row_'.$rowIndex][] = $this::FIRST_NAME_MISSING_ERROR_MESSAGE;
        }

        if (empty($row[$fields['last_name']])) {
            $this->errors['Row_'.$rowIndex][] = $this::LAST_NAME_MISSING_ERROR_MESSAGE;
        }

        if (empty($row[$fields['password']]) || strlen($row[$fields['password']]) < 8) {
            $this->errors['Row_'.$rowIndex][] = $this::PASSWORD_INVALID_ERROR_MESSAGE;
        }

        if (empty($row[$fields['platforms']]) ||strpos(strtolower($row[$fields['platforms']]), 'ios') === false && strpos(strtolower($row[$fields['platforms']]), 'windows') === false && strpos(strtolower($row[$fields['platforms']]), 'android') === false && strpos(strtolower($row[$fields['platforms']]), 'web') === false) {
            $this->errors['Row_'.$rowIndex][] = $this::PLATFORMS_INVALID_ERROR_MESSAGE;
        }

        if (count($this->errors['Row_'.$rowIndex]) > 0) {
            return false;
        } else {
            unset($this->errors['Row_'.$rowIndex]);
            return true;
        }

    }

    public function recordValidRow($rowIndex, $row, $fields, $validRows)
    {
        foreach ($fields as $field => $fieldIndex) {
            $validRows['Row_'.$rowIndex][$field] = $row[$fieldIndex] ?? '';
        }
        return $validRows;
    }

}
