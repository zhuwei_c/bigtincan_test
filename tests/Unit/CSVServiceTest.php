<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;

use App\Facades\CSVService;

class CSVServiceTest extends TestCase
{
    // @test
    public function testProcessCSVReturnDataInFile()
    {
        Storage::put('public/test.csv', null);
        $file = Storage::path('public/test.csv');
        $response = CSVService::processCSV($file);
        Storage::delete('public/test.csv');
        $this->assertTrue($response['success']);
        $this->assertTrue(is_array($response['message']));
    }

    // @test
    public function testProcessCSVReturnError()
    {
        $file = Storage::path('public/testError.csv');
        $response = CSVService::processCSV($file);
        Storage::delete('public/test.csv');
        $this->assertFalse($response['success']);
    }

    // @test
    public function testGetIndexesOfFieldsReturnError()
    {
        $response = CSVService::getIndexesOfFields([]);
        $this->assertFalse($response['success']);
    }

    // @test
    public function testGetIndexesOfFieldsReturnFieldsIndexes()
    {
        $fields = [
            'email',
            'first_name',
            'last_name',
            'password',
            'platforms'
        ];
        $response = CSVService::getIndexesOfFields($fields);
        $this->assertTrue($response['success']);
    }

    // @test
    public function testGenerateJSONFilesGenerated()
    {
        $tableData = [
            [
                'test@123.com',
                'test',
                'test',
                '12345678',
                'ios',
                'test'
            ]
        ];
        $fields = [
            'email' => 0,
            'first_name' => 1,
            'last_name' => 2,
            'password' => 3,
            'platforms' => 4,
            'test' => 5
        ];
        $response = CSVService::generateJSON($tableData, $fields, 'test_sample_users_successful.json', 'test_sample_users_unsuccessful.json');
        $this->assertTrue(file_exists(Storage::path('public/test_sample_users_successful.json')));
        $this->assertTrue(file_exists(Storage::path('public/test_sample_users_unsuccessful.json')));
        Storage::delete('public/test_sample_users_successful.json');
        Storage::delete('public/test_sample_users_unsuccessful.json');
    }

    // @test
    public function testRowValidationValid()
    {
        $validRow = [
            'test@123.com',
            'test',
            'test',
            '12345678',
            'ios',
            'test'
        ];
        $fields = [
            'email' => 0,
            'first_name' => 1,
            'last_name' => 2,
            'password' => 3,
            'platforms' => 4,
            'test' => 5
        ];
        $response = CSVService::rowValidation(0, $validRow, $fields);
        $this->assertTrue($response);
    }

    // @test
    public function testRowValidationInvalid()
    {
        $invalidEmailRow = [
            'test@123',
            'test',
            'test',
            '12345678',
            'ios',
            'test'
        ];
        $invalidFirstNameRow = [
            'test@123.com',
            '',
            'test',
            '12345678',
            'ios',
            'test'
        ];
        $invalidLastNameRow = [
            'test@123.com',
            'test',
            '',
            '12345678',
            'ios',
            'test'
        ];
        $invalidPasswordRow = [
            'test@123.com',
            'test',
            'test',
            '1234567',
            'ios',
            'test'
        ];
        $invalidPlatformsRow = [
            'test@123.com',
            'test',
            'test',
            '12345678',
            '',
            'test'
        ];
        $fields = [
            'email' => 0,
            'first_name' => 1,
            'last_name' => 2,
            'password' => 3,
            'platforms' => 4,
            'test' => 5
        ];
        $invalidEmailResponse = CSVService::rowValidation(0, $invalidEmailRow, $fields);
        $invalidFirstNameResponse = CSVService::rowValidation(0, $invalidFirstNameRow, $fields);
        $invalidLastNameResponse = CSVService::rowValidation(0, $invalidLastNameRow, $fields);
        $invalidPasswordResponse = CSVService::rowValidation(0, $invalidPasswordRow, $fields);
        $invalidPlatformsResponse = CSVService::rowValidation(0, $invalidPlatformsRow, $fields);
        $this->assertFalse($invalidEmailResponse);
        $this->assertFalse($invalidFirstNameResponse);
        $this->assertFalse($invalidLastNameResponse);
        $this->assertFalse($invalidPasswordResponse);
        $this->assertFalse($invalidPlatformsResponse);
    }

    // @test
    public function testRecordValidRow()
    {
        $row = [
            'test@123.com',
            'test',
            'test',
            '12345678',
            'ios',
            'test'
        ];
        $fields = [
            'email' => 0,
            'first_name' => 1,
            'last_name' => 2,
            'password' => 3,
            'platforms' => 4,
            'test' => 5
        ];
        $response = CSVService::recordValidRow(0, $row, $fields, []);
        $this->assertTrue(is_array($response));
        $this->assertTrue(!empty($response));
    }
}
